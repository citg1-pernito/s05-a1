package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {

	/* 
	 * 
	 */
	private static final long serialVersionUID = 43283931101411089L;
	/*
	 * Initialization - first stage of the servlet life cycle
	 * The "init" method is used to perform functions such as connecting to database and initializing values to be used in the context of the servlet
	 * - Loads the servlet
	 * - Creates an instance of the servlet class
	 * - Initializes the servlet instance by calling the init method
	 */
	public void init() throws ServletException {
		System.out.println("**********************");
		System.out.println("Initialized connection to database.");
		System.out.println("**********************");
	}
	
	/*
	 * The HttpServletRequest and HttpServletResponse objects should be imported from the jakarta package which is the java enterprise edition package
	 * Changing the method name to "doPost" ensure that this will only be accessible via "Post" request
	 * Service methods (doPost, service) - the second stage of the servlet lifecycle that handles the request and response
	 * Service() method - handle any type of request (recommended, and with the need to specify the actual method to receive the response)
	 */
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		/*
		 * Sysout then shift + space for autocomplete
		 */
		System.out.println("Hello from the calculator servlet.");
		
		/*
		 * The parameter names are defined in the form input field.
		 * The parameters are found in the url as query string (e.g. ?num1=&num2)
		 * getParameter() method is used to extract the parameters in a query string
		 */
		
		int num1 = Integer.parseInt(req.getParameter("num1"));
		int num2 = Integer.parseInt(req.getParameter("num2"));
		
		int total = num1 + num2;
//		The getWriter() method is used to print out information to the browser as a response
		PrintWriter out = res.getWriter();
		
//		Prints a string in the browser
//		getWriter can output not only string but also HTML elements
		out.println("<h1>The total of the two numbers are " +total+ "</h1>");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		PrintWriter out = res.getWriter();
		out.println("<h1>You have accessed the get method of the calculator servlet.</h1>");
	}
	
	/*
	 * Finalization - it is the last part of the servlet life cycle that invokes the "destroy" method
	 * - Clean up of the resources once the servlet is destroyed/unused
	 * - Closing the connections
	 */
	
	public void destroy() {
		System.out.println("**********************");
		System.out.println("Disconnected from database.");
		System.out.println("**********************");
	}
}
