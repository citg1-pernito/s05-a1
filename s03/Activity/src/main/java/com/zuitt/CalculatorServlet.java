package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {

	/* 
	 * 
	 */
	private static final long serialVersionUID = 43283931101411089L;
	/*
	 * Initialization - first stage of the servlet life cycle
	 * The "init" method is used to perform functions such as connecting to database and initializing values to be used in the context of the servlet
	 * - Loads the servlet
	 * - Creates an instance of the servlet class
	 * - Initializes the servlet instance by calling the init method
	 */
	public void init() throws ServletException {
		System.out.println("**********************");
		System.out.println("Initialized connection to database.");
		System.out.println("**********************");
	}
	
	/*
	 * The HttpServletRequest and HttpServletResponse objects should be imported from the jakarta package which is the java enterprise edition package
	 * Changing the method name to "doPost" ensure that this will only be accessible via "Post" request
	 * Service methods (doPost, service) - the second stage of the servlet lifecycle that handles the request and response
	 * Service() method - handle any type of request (recommended, and with the need to specify the actual method to receive the response)
	 */
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		/*
		 * Sysout then shift + space for autocomplete
		 */
		System.out.println("Hello from the calculator servlet.");
		
		/*
		 * The parameter names are defined in the form input field.
		 * The parameters are found in the url as query string (e.g. ?num1=&num2)
		 * getParameter() method is used to extract the parameters in a query string
		 */
		
		int num1 = Integer.parseInt(req.getParameter("num1"));
		int num2 = Integer.parseInt(req.getParameter("num2"));
		int operation = Integer.parseInt(req.getParameter("operation"));
		
		if(operation == 1) {
			int total = num1 + num2;
			PrintWriter out = res.getWriter();
			out.println("You have inputted: " + num1 + " + " + num2 + ".");
			out.println("The sum of the two numbers is: " + total + ".");
		}
		else if(operation == 2) {
			int total = num1 - num2;
			PrintWriter out = res.getWriter();
			out.println("You have inputted: " + num1 + " - " + num2 + ".");
			out.println("The difference of the two numbers is: " + total + ".");
		}
		else if(operation == 3) {
			int total = num1 * num2;
			PrintWriter out = res.getWriter();
			out.println("You have inputted: " + num1 + " � " + num2 + ".");
			out.println("The product of the two numbers is: " + total + ".");
		}
		else if(operation == 4) {
			int total = num1 / num2;
			PrintWriter out = res.getWriter();
			out.println("You have inputted: " + num1 + " � " + num2 + ".");
			out.println("The quotient of the two numbers is: " + total + ".");
		}
		else {
			PrintWriter out = res.getWriter();
			out.println("<h1>You have inputted a wrong value in the third textbox, please review the instructions provided. Thank You.</h1>");
		}
		
//		int total = num1 + num2;
//		The getWriter() method is used to print out information to the browser as a response
//		PrintWriter out = res.getWriter();
		
//		Prints a string in the browser
//		getWriter can output not only string but also HTML elements
//		out.println("<h1>The total of the two numbers are " +total+ "</h1>");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		PrintWriter out = res.getWriter();
		out.println("<h1>Thank you for using my calculator app, please read the instructions below on how to use the calculator app :</h1>");
		out.println("To use the app, please input two numbers and an operation of your choice.<br><br>");
		out.println("Type 1 for addition, type 2 for subtraction, type 3 for multiplication, and type 4 for division.<br><br>");
		out.println("Then, hit the submit button once you are satisfied with your inputs.<br><br>");
		out.println("The results will be provided after you have successfully clicked the submit button.");
	}
	
	/*
	 * Finalization - it is the last part of the servlet life cycle that invokes the "destroy" method
	 * - Clean up of the resources once the servlet is destroyed/unused
	 * - Closing the connections
	 */
	
	public void destroy() {
		System.out.println("**********************");
		System.out.println("Disconnected from database.");
		System.out.println("**********************");
	}
}
